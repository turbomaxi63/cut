﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public enum EgameState
{
    menu = 0,
    game = 1,
    gameover = 2,
    start = 3,
    finish = 4,
}
public class MoveCUT : MonoBehaviour
{
    public Vector3 forceSize;
    Rigidbody rigidBodySaw;
    public EgameState gameState;
    [SerializeField]
    private GameObject gameOverText;
    [SerializeField]
    private GameObject gameStartText;
    [SerializeField]
    private GameObject gameFinishText;
    [SerializeField]
    private Transform SAW;
    private float touchdelay;
    private bool istouch;
    [SerializeField]
    private float touchduration = 0.5f;
    //Счет объектов для выигрыша
    private int baseCount;

    //Увеличение пилы 
    [SerializeField]
    private Transform nowSaw;
    [SerializeField]
    private float increaseSizeSaw = 0.0f;
    [SerializeField]
    private float standartSizeSaw = 0.0f;
    [SerializeField]
    private float MaxSizeSaw = 0.0f;
    //Уменьшение пилы
    [SerializeField]
    private float decreaseSizesaw = 0.0f;
    [SerializeField]
    private float MinSizeSaw = 0.0f;
    [SerializeField]

    private Rotate RotateCylinder;
    [SerializeField]
    private float WoodRotate = 0f;
    [SerializeField]




    // Start is called before the first frame update
    void Start()
    {
        rigidBodySaw = GetComponent<Rigidbody>();
        gameState = EgameState.menu;
#if UNITY_IOS || UNITY_ANDROID
        Application.targetFrameRate = 60;
#endif
    }
    // Update is called once per frame
    void Update()
    {
        if (gameState == EgameState.menu)
        {
            RotateCylinder.enabled = false;
            if (Input.GetMouseButtonDown(0) || (Input.GetKeyDown(KeyCode.Space)))
            {
                gameState = EgameState.start;
            }

        }

        else if (gameState == EgameState.start)
        {
            SAW.Rotate(100f * Time.deltaTime, 0, 0);
            gameStartText.SetActive(true);
            baseCount = GameObject.FindGameObjectsWithTag("Cut").Length;
            transform.localScale = new Vector3(standartSizeSaw, standartSizeSaw, standartSizeSaw);

            if (Input.GetMouseButtonDown(0) || (Input.GetKeyDown(KeyCode.Space)))
            {

                rigidBodySaw.isKinematic = false;
                //Debug.Log(rigidBodySaw.name);
                gameState = EgameState.game;
                gameStartText.SetActive(false);
            }
        }

        else if (gameState == EgameState.game)
        {
            RotateCylinder.enabled = true;
            SAW.Rotate(100f * Time.deltaTime, 0, 0);



            if (Input.GetMouseButtonDown(0) || (Input.GetKeyDown(KeyCode.Space)))
            {
                Jump();
            }
            else if (Input.GetMouseButtonUp(0) || (Input.GetKeyUp(KeyCode.Space)))
            {
                istouch = false;
                touchdelay = 0f;
            }
            if (Input.GetMouseButton(0) || (Input.GetKey(KeyCode.Space)))
            {
                if (istouch)
                {
                    touchdelay += Time.deltaTime;
                }
            }
        }

        else if (gameState == EgameState.gameover || gameState == EgameState.finish)
        {
            if (Input.GetMouseButtonDown(0) || (Input.GetKeyDown(KeyCode.Space)))
                SceneManager.LoadScene(0);
            RotateCylinder.enabled = false;
        }
    }

    private void FixedUpdate()
    {
        if (gameState == EgameState.game)
        {
            Fly();
            // Debug.Log("Fly " + rigidBodySaw.velocity);
        }
    }

    void OnCollisionEnter(Collision other)
    {
        // Debug.Log("Crash");
        if (gameState == EgameState.game)
        {
            if (other.collider.CompareTag("Stone"))
            {
                EndGame(false);
            }
        }
    }
    [SerializeField]
    private ParticleSystem SawCrashParticles;
    void EndGame(bool win)
    {
        rigidBodySaw.isKinematic = true;
        rigidBodySaw.velocity = new Vector2(0f, 0f);
        if (win)
        {
            gameState = EgameState.finish;        
            gameFinishText.SetActive(true);
        }

        else
        {
            gameState = EgameState.gameover;
            gameOverText.SetActive(true);
            SawCrashParticles.transform.position = transform.position;
            SawCrashParticles.Play();
            transform.GetComponent<MeshRenderer>().enabled = false;
        }
    }

    [SerializeField]
    private ParticleSystem cutting;
    private ParticleSystem particleCutWood;
    [SerializeField]
    private ParticleSystem BoostIncrease;
    private ParticleSystem ParticleBoostincrease;
    [SerializeField]
    private ParticleSystem StoneDecrease;
    private ParticleSystem ParticleBoosDecrease;
    void OnTriggerEnter(Collider other)
    {
        if (gameState == EgameState.game)
        {
            if (other.CompareTag("Cut"))
            {
                //Debug.Log("particl");
                other.transform.parent = null;
                particleCutWood = Instantiate<ParticleSystem>(cutting);
                particleCutWood.transform.position = new Vector3(other.transform.position.x, other.transform.position.y, other.transform.position.z);
                particleCutWood.Play();
                Rigidbody rb = other.GetComponent<Rigidbody>();
                if (rb)
                {
                    rb.isKinematic = false;
                    // Debug.Log(other.name);

                }
                Destroy(other.gameObject);       



            }
            
            if (GameObject.FindGameObjectsWithTag("Cut").Length / (float)baseCount <= 0.9)
            {
                         
             EndGame(true);
            }

            if (other.CompareTag("Increase"))
            {
                Vector3 localScale = nowSaw.localScale;
                if (MaxSizeSaw >= localScale.x)
                {
                    transform.localScale += new Vector3(increaseSizeSaw, increaseSizeSaw, increaseSizeSaw);
                }
                //Debug.Log(MaxSizeSaw);
                // Debug.Log(localScale.x);
                other.transform.parent = null;
                ParticleBoostincrease = Instantiate<ParticleSystem>(BoostIncrease);
                ParticleBoostincrease.transform.position = new Vector3(other.transform.position.x, other.transform.position.y, -1);
                ParticleBoostincrease.Play();

                if (MaxSizeSaw > standartSizeSaw)
                {
                    Destroy(other.gameObject);
                }


            }

            if (other.CompareTag("Decrease"))
            {
                Vector3 localScale = nowSaw.localScale;
                if (MinSizeSaw < localScale.x)
                {
                    transform.localScale -= new Vector3(decreaseSizesaw, decreaseSizesaw, decreaseSizesaw);
                }
                //Debug.Log(decreaseSizesaw);
                //Debug.Log(localScale.x);
                //Debug.Log(standartSizeSaw);
                other.transform.parent = null;
                ParticleBoosDecrease = Instantiate<ParticleSystem>(StoneDecrease);
                ParticleBoosDecrease.transform.position = new Vector3(other.transform.position.x, other.transform.position.y, -1);
                ParticleBoosDecrease.Play();
                Destroy(other.gameObject);
                if (MinSizeSaw >= localScale.x)
                {
                    EndGame(true);
                }

            }

        }

    }
    private void Jump()
    {
        istouch = true;
        touchdelay = 0f;
        rigidBodySaw.velocity = rigidBodySaw.angularVelocity = Vector3.zero;
        rigidBodySaw.AddForce(forceSize);
        //Debug.Log("Jump");
    }
    private void Fly()
    {
        if (touchdelay > touchduration)
        {
            rigidBodySaw.velocity = rigidBodySaw.angularVelocity = Vector3.zero;
            rigidBodySaw.AddForce(forceSize);
            //Debug.Log("Fly " + rigidBodySaw.velocity);
        }
    }
}


