﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PilaDestroy : MonoBehaviour
{
    // Start is called before the first frame update
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Increase"))
        {
            Destroy(other.gameObject);
            Debug.Log("Уничтожить");
        }

    }
}
